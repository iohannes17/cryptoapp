//
//  App.js
//  Cryptonance
//
//  Created by Supernova.
//  Copyright © 2018 Supernova. All rights reserved.
//

import * as Font from "expo-font";
import React from "react";
import { AppLoading } from "expo";
import SignedOutNavigator from "navigation/SignedOutNavigator";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontsReady: false,
    };
  }

  componentDidMount() {
    this.initProjectFonts();
  }

  async initProjectFonts() {
    await Font.loadAsync({
      "Lato-Bold": require("./assets/fonts/LatoBold.ttf"),
      ".SFNSText": require("./assets/fonts/SFNSText.ttf"),
      "Lato-Black": require("./assets/fonts/LatoBlack.ttf"),
      "Lato-Regular": require("./assets/fonts/LatoRegular.ttf"),
    });
    this.setState({
      fontsReady: true,
    });
  }

  render() {
    if (!this.state.fontsReady) {
      return <AppLoading />;
    }
    return (
      <NavigationContainer>
        <Stack.Navigator headerMode={"none"}>
          <Stack.Screen name={"signedOut"} component={SignedOutNavigator} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

// import "react-native-gesture-handler";
// import * as React from "react";
// import { NavigationContainer } from "@react-navigation/native";

// export default function App() {
//   return (
//     <NavigationContainer>{/* Rest of your app code */}</NavigationContainer>
//   );
// }
