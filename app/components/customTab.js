import React, { Fragment } from "react";
import { View, ScrollView, StyleSheet, Pressable } from "react-native";
import Animated from "react-native-reanimated";
import {
  globalStyles,
  fontConfig,
  StyledTheme as theme,
} from "app/config/theme";

const CustomTabBar = ({
  state,
  descriptors,
  navigation,
  position,
  headerLeft,
  headerRight,
  headerStyle = {},
}) => {
  return (
    <View style={[styles.tabBarWrapper, headerStyle]}>
      {headerLeft ? (
        <Fragment>{headerLeft()}</Fragment>
      ) : (
        <View style={styles.spacer} />
      )}
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[styles.tabBar]}
      >
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: "tabPress",
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: "tabLongPress",
              target: route.key,
            });
          };

          const opacity = isFocused ? 1 : 0.25;
          const focusedTabStyle = isFocused
            ? { borderBottomColor: theme.colors.black }
            : {};
          return (
            <Pressable
              key={index}
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              style={[styles.tab, focusedTabStyle]}
              onLongPress={onLongPress}
            >
              <Animated.Text style={[styles.tabLabelText, { opacity }]}>
                {label}
              </Animated.Text>
            </Pressable>
          );
        })}
      </ScrollView>
      {headerRight ? <Fragment>{headerRight()}</Fragment> : <Fragment />}
    </View>
  );
};

const styles = StyleSheet.create({
  tabBarWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomColor: theme.colors.lightGrey,
    borderBottomWidth: 0.5,
  },
  tabBar: {
    flexDirection: "row",
    paddingHorizontal: 0,
    flex: 1,
  },
  profile: {
    padding: 16,
    paddingVertical: 8,
    justifyContent: "center",
    alignItems: "center",
  },

  tab: {
    marginRight: 12,
    paddingVertical: 16,
    borderBottomWidth: 0,
    borderBottomColor: theme.colors.transparent,
  },
  tabLabelText: {
    fontFamily: fontConfig.default.bold.fontFamily,
    fontWeight: fontConfig.default.bold.fontWeight,
    textTransform: "capitalize",
    letterSpacing: -0.5,
    fontSize: 22,
    padding: 0,
    color: theme.colors.black,
  },
  spacer: {
    width: 16,
  },
});

export default CustomTabBar;
