// list your chat history
import React, {
  useState,
  useRef,
  Fragment,
  useEffect,
  useCallback,
} from "react";
import { Layout, Spacer } from "app/components";
import {
  globalStyles,
  fontConfig,
  StyledTheme as theme,
} from "app/config/theme";

const Page = ({}) => {
  return (
    <Layout edges={["left", "right"]} style={[globalStyles.layout]}></Layout>
  );
};

const inStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
});

export default withInAppNotification(Page);
