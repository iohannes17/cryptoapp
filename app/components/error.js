// list your chat history
import React, {
  useState,
  useRef,
  Fragment,
  useEffect,
  useCallback,
} from "react";
import { Layout, Spacer } from "app/components";
import {
  View,
  ScrollView,
  StyleSheet,
  Pressable,
  FlatList,
} from "react-native";

import {
  Button,
  Subheading,
  Headline,
  Paragraph,
  TextInput,
  Text,
  List,
  Colors,
  Portal,
  FAB,
  Dialog,
  ActivityIndicator,
} from "react-native-paper";
import {
  globalStyles,
  fontConfig,
  StyledTheme as theme,
} from "app/config/theme";

const Page = ({}) => {
  const [defaultNetworkMessage, setdefaultNetworkMessage] = useState(
    "Bad Request"
  );

  return (
    <Layout edges={["left", "right"]} style={globalStyles.layout}></Layout>
  );
};

const styles = StyleSheet.create({
  errorContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  retry: { borderRadius: 25 },
  errorText: {
    textAlign: "center",
  },
  loadingStyle: {
    flexDirection: "row",
  },

  messageStyle: {
    textTransform: "capitalize",
    fontFamily: fontConfig.default.medium.fontFamily,
    fontWeight: fontConfig.default.medium.fontWeight,
  },
});

export default Page;
