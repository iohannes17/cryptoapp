// start new chat by searching for users you would like to communicate with
import React, { useState, useEffect, useCallback, Fragment } from "react";
import { Layout, Spacer } from "app/components";
import {
  View,
  ScrollView,
  StyleSheet,
  Pressable,
  RefreshControl,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  TouchableWithoutFeedback,
  Vibration,
  Image,
} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {
  Button,
  Banner,
  List,
  Paragraph,
  Snackbar,
  Caption,
} from "react-native-paper";

const Page = ({
  navigation,
  onRetry,
  onError,
  title = "",
  subTitle = undefined,
  dateTitle,
}) => {
  return (
    <Layout
      style={[globalStyles.layout, styles.layout]}
      edges={["left", "right", "bottom"]}
    ></Layout>
  );
};

export default Page;
