import React from 'react';
import { View, StyleSheet } from 'react-native';
import { StyledTheme as theme } from 'app/config/theme';
import { ActivityIndicator } from 'react-native-paper';

const Page = ({ showLoading }) => {
    return (
        <View style={styles.loadingStyle}>
            {showLoading && <ActivityIndicator animating={true} color={theme.colors.primary} />}
        </View>
    );
};

const styles = StyleSheet.create({
    loadingStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
});

export default Page;
