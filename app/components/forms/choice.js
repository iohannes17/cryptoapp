import React, { Fragment, useState, useCallback, useEffect } from 'react';
import { View, Modal, Pressable, StyleSheet, FlatList, TouchableOpacity } from 'react-native';
import { Field } from 'formik';
import { TextInput, HelperText, Appbar, List, Colors, Avatar, Paragraph, ActivityIndicator } from 'react-native-paper';
import { Layout, Spacer } from 'app/components';
import { StyledTheme as theme, globalStyles, fontConfig } from 'app/config/theme';
import Fuse from 'fuse.js';
import _ from 'lodash';

const DefaultModalContent = ({
    options = [],
    nameProp = 'name',
    valueProp = 'pk',
    captionProp = 'description',
    titleFormat,
    captionFormat,
    titleNumberOfLines = 1,
    captionNumberOfLines = 1,
    onItemSelected,
    extraData,
    listItemStyle = {},
}) => {
    const getTitle = (item) => {
        const title = item[nameProp] || '';
        return titleFormat ? titleFormat(title, item) : title;
    };

    const getCaption = (item) => {
        const caption = item[captionProp] || '';
        return captionFormat ? captionFormat(caption, item) : caption;
    };

    const getAvatar = (item) => {
        const title = getTitle(item);
        return title ? title[0] : 'U';
    };

    const keyExtractor = (item, idx) => `${idx}`;

    return (
        <FlatList
            style={styles.list}
            data={options}
            extraData={extraData}
            keyExtractor={keyExtractor}
            ItemSeparatorComponent={() => <Spacer style={[globalStyles.separator, styles.separator]} />}
            renderItem={({ item, index, separator }) => (
                <List.Item
                    key={index}
                    onPress={() => onItemSelected && onItemSelected(item)}
                    title={getTitle(item)}
                    titleNumberOfLines={titleNumberOfLines}
                    descriptionNumberOfLines={captionNumberOfLines}
                    style={[styles.listItem, listItemStyle]}
                    titleStyle={styles.listTitleStyle}
                    descriptionStyle={styles.listDescriptionStyle}
                    description={getCaption(item)}
                    // left={(props) => (
                    //     <Avatar.Text
                    //         size={36}
                    //         color={theme.colors.black}
                    //         label={getAvatar(item)}
                    //         style={styles.avatar}
                    //         labelStyle={styles.avatarLabel}
                    //     />
                    // )}
                />
            )}
        />
    );
};

export const ChoiceInput = ({
    pressableStyle = {},
    wrapperStyle = {},
    onItemSelected,
    onBlur,
    onClose,
    modalPresentationStyle = 'fullScreen',
    modalAnimationType = 'slide',
    statusBarTranscluscent = true,
    modalEdges,
    modalStyle = {},
    listItemStyle = {},
    options = [],
    nameProp = 'name',
    valueProp = 'pk',
    searchKeys = ['name'],
    searchFunction,
    loading = false,
    showDropdown = true,
    captionProp = 'description',
    titleFormat,
    captionFormat,
    value,
    right = (
        <TextInput.Icon
            name={'chevron-down'}
            // eslint-disable-next-line react-native/no-inline-styles
            style={styles.rightIconStyle}
        />
    ),
    ModalContent = DefaultModalContent,
    searchPlaceholder = 'Type to search',
    hasErrors,
    style,
    disabled = false,
    ...props
}) => {
    const [visible, setVisible] = useState(false);
    const [searchInput, setSearchInput] = useState('');
    const [filteredOptions, setFilteredOptions] = useState(options);

    useEffect(() => {
        setFilteredOptions(options);
    }, [options]);

    const getOptions = ({ searchInput: input = '' }) => {
        const fuse = new Fuse(options, {
            keys: searchKeys,
            includeScore: true,
            useExtendedSearch: true,
        });

        if (_.isEmpty(input)) {
            setFilteredOptions(options);
            return;
        }

        const res = fuse.search(`'${input}`).map((elem) => elem.item); // items that include the particular phrase
        console.log({ fuse, options, input, searchKeys, res });
        setFilteredOptions(res);
    };

    const onSelected = (item) => {
        onItemSelected && onItemSelected(item);
        closeModal();
        onSearchInputChanged('');
    };

    const closeModal = () => {
        setVisible(false);
        onClose && onClose();
    };

    const handleSearch = searchFunction || getOptions;

    const onSearchInputChanged = (val) => {
        setSearchInput(val);
        handleSearch({
            options,
            searchInput: val,
            searchKeys,
        });
    };

    const placeholderStyle = value ? {} : globalStyles.placeholderInput;

    return (
        <Fragment>
            <View style={[styles.wrapper, wrapperStyle]}>
                <TextInput
                    value={value}
                    editable={false}
                    onBlur={onBlur}
                    // numberOfLines={1}
                    scrollEnabled={false}
                    right={
                        hasErrors ? (
                            <TextInput.Icon
                                name={'alert-circle'}
                                style={styles.rightIconStyle}
                                color={theme.colors.red}
                            />
                        ) : showDropdown ? (
                            right
                        ) : null
                    }
                    style={[style, placeholderStyle]}
                    {...props}
                />
                <Pressable
                    onPress={() => {
                        !disabled && setVisible(true);
                    }}
                    style={[styles.pressable, pressableStyle]}
                />
            </View>
            <Modal
                visible={visible}
                animationType={modalAnimationType}
                hardwareAccelerated={true}
                presentationStyle={modalPresentationStyle}
                statusBarTranscluscent={statusBarTranscluscent}
                onRequestClose={closeModal}>
                <Layout edges={['left', 'right']} style={styles.layout}>
                    <Appbar.Header style={styles.header}>
                        <Appbar.Action icon={'chevron-down'} onPress={closeModal} />
                        <TextInput
                            dense
                            value={searchInput}
                            onChangeText={onSearchInputChanged}
                            underlineColor={'transparent'}
                            selectionColor={theme.colors.primary}
                            autoFocus={true}
                            theme={{
                                colors: { primary: 'transparent' },
                            }}
                            placeholder={searchPlaceholder}
                            style={styles.searchInput}
                            right={
                                loading ? (
                                    <TextInput.Affix
                                        text={
                                            <ActivityIndicator
                                                animating={true}
                                                size={20}
                                                style={styles.indicator}
                                                color={theme.colors.textGrey}
                                            />
                                        }
                                    />
                                ) : searchInput ? (
                                    <TextInput.Icon
                                        onPress={() => setSearchInput('')}
                                        name={'close-circle'}
                                        color={'#c2c2c2'}
                                    />
                                ) : null
                            }
                        />
                    </Appbar.Header>
                    <ModalContent
                        options={filteredOptions}
                        nameProp={nameProp}
                        valueProp={valueProp}
                        captionProp={captionProp}
                        titleFormat={titleFormat}
                        captionFormat={captionFormat}
                        listItemStyle={listItemStyle}
                        value={value}
                        extraData={searchInput}
                        onItemSelected={onSelected}
                        {...props}
                    />
                </Layout>
            </Modal>
        </Fragment>
    );
};

const FieldComponent = ({
    field: { name, value, onChange, onBlur },
    form: { errors, touched, setFieldValue, setFieldTouched },
    format,
    ...props
}) => {
    const handleChange = onChange(name);
    const handleBlur = onBlur(name);
    const hasErrors = touched[name] && errors[name];
    const { valueProp, onItemSelected } = props;

    const onSelect = (item) => {
        const val = item[valueProp];
        setFieldValue(name, val, true);
        // onItemSelected && onItemSelected(item);
        setTimeout(() => {
            onItemSelected && onItemSelected(item);
            onClose();
        }, 10);
        // setFieldTouched(name, true);
    };

    const onClose = () => setFieldTouched(name, true);

    const _formatValue = (val) => {
        return format ? format(val) : val;
    };

    return (
        <Fragment>
            <ChoiceInput
                value={_formatValue(value)}
                onChangeText={handleChange}
                onBlur={handleBlur}
                onClose={onClose}
                hasErrors={hasErrors}
                {...props}
                onItemSelected={onSelect}
            />
            {hasErrors && (
                <HelperText padding={'none'} type={'error'} visible={hasErrors}>
                    {touched[name] && errors[name]}
                </HelperText>
            )}
        </Fragment>
    );
};

const InputField = ({ name, ...props }) => {
    return <Field name={name} component={FieldComponent} {...props} />;
};

const styles = StyleSheet.create({
    layout: {
        backgroundColor: theme.colors.white,
    },
    wrapper: {},
    indicator: {
        alignSelf: 'center',
    },
    pressable: {
        position: 'absolute',
        backgroundColor: 'transparent',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        zIndex: 100,
    },
    separator: {
        marginLeft: 20,
    },
    searchInput: {
        flex: 1,
        backgroundColor: 'transparent',
        paddingHorizontal: 0,
    },
    avatar: {
        backgroundColor: '#f8f8f8',
        alignSelf: 'center',
        marginHorizontal: 8,
    },
    avatarLabel: {
        fontWeight: '600',
    },
    list: {
        backgroundColor: theme.colors.white,
        borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: theme.colors.borderColor,
    },
    header: {
        backgroundColor: theme.colors.white,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: theme.colors.borderColor,
    },
    listItem: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 16,
    },
    listTitleStyle: {
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
        fontSize: 16,
        lineHeight: 24,
    },
    listDescriptionStyle: {
        fontFamily: fontConfig.default.regular.fontFamily,
        fontWeight: fontConfig.default.regular.fontWeight,
        fontSize: 14,
        lineHeight: 24,
        letterSpacing: 0,
        color: theme.colors.textGrey,
    },
    rightIconStyle: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        alignSelf: 'center',
        marginRight: -12,
    },
});

export default InputField;
