import React, { Fragment } from 'react';
import { View } from 'react-native';
import { Field } from 'formik';
import { TextInput, HelperText, Colors, ActivityIndicator } from 'react-native-paper';
import { globalStyles, fontConfig, StyledTheme as theme } from 'app/config/theme';

const FieldComponent = ({
    field: { name, value, onChange, onBlur },
    form: { errors, touched },
    format,
    showErrorIcon = true,
    errorIconStyle = {
        // alignItems: 'flex-end',
        // justifyContent: 'flex-end',
        // marginRight: -16,
    },
    onInputChanged,
    innerRef,
    style = {},
    ...props
}) => {
    const handleChange = onChange(name);
    const handleBlur = onBlur(name);
    const hasErrors = touched[name] && errors[name];

    const _formatValue = (val) => {
        return format ? format(val) : val;
    };

    const _onChangeText = (val) => {
        handleChange(val);
        onInputChanged && onInputChanged(val);
    };

    const placeholderStyle = value ? {} : globalStyles.placeholderInput;

    return (
        <Fragment>
            <TextInput
                ref={innerRef}
                value={_formatValue(value)}
                onChangeText={_onChangeText}
                onBlur={handleBlur}
                {...props}
                style={[style, placeholderStyle]}
            />
            {hasErrors && (
                <HelperText padding={'none'} type={'error'} style={globalStyles.errorText} visible={hasErrors}>
                    {touched[name] && errors[name]}
                </HelperText>
            )}
        </Fragment>
    );
};

const InputField = ({ name, ...props }) => {
    return <Field name={name} component={FieldComponent} {...props} />;
};

export default InputField;
