import React, { useState } from 'react';
import { View, Pressable, StyleSheet } from 'react-native';
import { Field } from 'formik';
import { Text } from 'react-native-paper';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { StyledTheme as theme } from 'app/config/theme';

const KeypadBackspace = (props) => <FeatherIcon name={'arrow-left'} {...props} />;

export const VirtualKeyboard = ({
    pressMode = 'string',
    color = theme.colors.black,
    rowBackgroundColor = 'transparent',
    keyBackgroundColor = 'transparent',
    fontWeight = '400',
    fontSize = theme.sizes.medium,
    BackspaceComponent = KeypadBackspace,
    applyBackspaceTint = true,
    decimal = false,
    activeOpacity = 1,
    keypadType = 'numeric',
    value = '',
    onPress,
    ...props
}) => {
    const [text, setText] = useState(value);

    const getSpecialCharacter = (padType) => {
        let char = '';
        switch (padType) {
            case 'numeric':
                char = '.';
                break;
            case 'phone':
                char = '+';
                break;
            default:
                char = '';
                break;
        }
        return char;
    };

    const validateInput = (val) => {
        const { validator } = props;
        if (validator && !validator(val)) {
            return false;
        }
        return true;
    };

    const specialCharacter = getSpecialCharacter(keypadType);

    const _onChange = (val) => {
        console.log('inside on change', { val });
        onPress && onPress(val);
        setText(val);
    };

    const generateRow = (numbersArray) => {
        let cells = numbersArray.map((val) => generateKey(val));
        return <View style={styles.row}>{cells}</View>;
    };

    const generateKey = (symbol) => {
        return (
            <Pressable
                style={styles.key}
                key={symbol}
                accessibilityLabel={symbol.toString()}
                onPress={() => {
                    onKeyPress(symbol.toString());
                }}>
                {({ pressed }) => (
                    <Text style={pressed ? [styles.character, { opacity: activeOpacity }] : styles.character}>
                        {symbol}
                    </Text>
                )}
            </Pressable>
        );
    };

    const generateBackspace = () => {
        return (
            <Pressable
                style={styles.key}
                accessibilityLabel="backspace"
                onPress={() => {
                    onKeyPress('back');
                }}>
                {({ pressed }) => <BackspaceComponent color={color} size={fontSize} />}
            </Pressable>
        );
    };

    const onKeyPress = (val) => {
        const { format } = props;
        if (pressMode === 'string') {
            let curText = text;
            if (isNaN(val)) {
                if (val === 'back') {
                    curText = curText.slice(0, -1);
                    _onChange(curText);
                } else {
                    // Check if special character is already present
                    const spec = getSpecialCharacter(keypadType);
                    console.log({ curText, spec, val });
                    if (val === spec && !curText.includes(spec)) {
                        curText += val;
                    }
                }
            } else {
                curText += val;
                // Check if character is valid to specified decimal places
            }
            if (validateInput(curText)) {
                console.log('current input is valid', { isValid: validateInput(curText) });
                const formatted = format ? format(curText) : curText;
                _onChange(formatted);
            }
        } /* if (props.pressMode == 'char')*/ else {
            _onChange(val);
        }
    };

    return (
        <View style={styles.container}>
            {generateRow([1, 2, 3])}
            {generateRow([4, 5, 6])}
            {generateRow([7, 8, 9])}
            <View style={styles.row}>
                {decimal ? generateKey(specialCharacter) : <View style={styles.key} />}
                {generateKey(0)}
                {generateBackspace()}
            </View>
        </View>
    );
};

const FieldComponent = ({
    field: { name, value, onChange },
    // form: { errors, touched },
    format,
    ...props
}) => {
    const handleChange = onChange(name);
    // const handleBlur = onBlur(name);
    // const hasErrors = touched[name] && errors[name];

    const _formatValue = (val) => {
        return format ? format(val) : val;
    };

    return <VirtualKeyboard value={_formatValue(value)} onPress={handleChange} format={format} {...props} />;
};

const InputField = ({ name, ...props }) => {
    return <Field name={name} component={FieldComponent} {...props} />;
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignContent: 'stretch',
    },
    row: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent: 'stretch',
        alignItems: 'stretch',
        minHeight: theme.sizes.keyHeight,
    },
    key: {
        justifyContent: 'center',
        alignItems: 'center',
        // margin: -1,
        flex: 1,
        alignSelf: 'stretch',
        padding: 10,
    },
    character: {
        fontSize: theme.sizes.medium,
        fontWeight: '400',
        color: theme.colors.black,
    },
    pressed: {},
});

export default InputField;
