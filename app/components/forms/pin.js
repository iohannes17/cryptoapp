import React, { Fragment } from 'react';
import { View, StyleSheet } from 'react-native';
import { Field } from 'formik';
import { Spacer } from 'app/components';
import { TextInput, HelperText, Colors } from 'react-native-paper';
import PinCodeInput from 'react-native-smooth-pincode-input';
import { fontConfig, StyledTheme as theme } from 'app/config/theme';

const FieldComponent = ({
    field: { name, value, onChange, onBlur },
    form: { errors, touched },
    format,
    showErrorIcon = true,
    errorIconStyle = {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginRight: -16,
    },
    containerStyle = styles.containerStyle,
    cellStyle = styles.cellStyle,
    cellStyleFocused = styles.cellStyleFocused,
    textStyle = styles.textStyle,
    textStyleFocused = styles.textStyleFocused,
    onComplete,
    ...props
}) => {
    const handleChange = onChange(name);
    const handleBlur = onBlur(name);
    const hasErrors = touched[name] && errors[name];

    const _formatValue = (val) => {
        return format ? format(val) : val;
    };

    const onFulfilled = (val) => {
        onComplete && onComplete(val);
    };

    return (
        <Fragment>
            <PinCodeInput
                value={_formatValue(value)}
                onTextChange={handleChange}
                textStyle={textStyle}
                textStyleFocused={textStyleFocused}
                cellStyle={cellStyle}
                cellStyleFocused={cellStyleFocused}
                containerStyle={containerStyle}
                // onBlur={handleBlur}
                onFulfill={onFulfilled}
                {...props}
            />
            <Spacer height={8} />
            {/* <HelperText padding={'none'} type={'error'} visible={hasErrors}>
                {touched[name] && errors[name]}
            </HelperText> */}
        </Fragment>
    );
};

const PinField = ({ name, ...props }) => {
    return <Field name={name} component={FieldComponent} {...props} />;
};

const styles = StyleSheet.create({
    containerStyle: {
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    cellStyle: {
        borderBottomWidth: 2,
        borderColor: theme.colors.lightGrey,
        width: 32,
        marginRight: 12,
    },
    cellStyleFocused: {
        borderBottomWidth: 2,
        borderColor: theme.colors.primary,
        backgroundColor: theme.colors.backgroundGrey,
    },
    textStyle: {
        fontSize: 24,
        fontFamily: fontConfig.default.regular.fontFamily,
        fontWeight: fontConfig.default.regular.fontWeight,
    },
    textStyleFocused: {},
});

export default PinField;
