import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import styled, { css } from "styled-components/native";
import ErrorPage from "./error";
import { ActivityIndicator } from "react-native-paper";
import { View } from "react-native";
import { StyledTheme as theme } from "app/config/theme";
export const Layout = styled(SafeAreaView)`
  flex: ${(props) => props.flex || 1};
  background-color: ${(props) =>
    props.backgroundColor || theme.layout.backgroundColor};
`;

export const FooterComponent = ({ showLoading }) => {
  return (
    <View style={styles.footer}>
      {showLoading && (
        <ActivityIndicator animating={true} color={theme.colors.primary} />
      )}
    </View>
  );
};

const styles = {
  footer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
};

export const Spacer = styled.View`
  ${(props) =>
    props.height &&
    css`
      height: ${`${props.height}px`};
    `};
  ${(props) =>
    props.width &&
    css`
      width: ${`${props.width}px`};
    `};
`;

export { ErrorPage };
