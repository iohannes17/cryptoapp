import React from 'react';
import { Layout } from './index';
import { StyleSheet } from 'react-native';
import _ from 'lodash';

import { ActivityIndicator } from 'react-native-paper';
import { StyledTheme as theme } from 'app/config/theme';

const Page = ({}) => {
    return (
        <Layout style={styles.layout} edges={['left', 'right', 'bottom']}>
            <ActivityIndicator color={theme.colors.primary} size={'small'} />
        </Layout>
    );
};

const styles = StyleSheet.create({
    layout: {
        backgroundColor: theme.colors.white,
        justifyContent: 'center',
        alignItems: 'center',
        opacity: 1,
    },
});

export default Page;
