// /** New setup for creating apollo client with login and logout workflow */
// import { ApolloClient, HttpLink, ApolloLink, InMemoryCache, from } from '@apollo/client';

// import settings from './settings';
// import { onError } from '@apollo/client/link/error';
// import { detectErrors } from '../lib/errorHandlers';
// import { offsetLimitPagination, concatPagination, relayStylePagination } from '@apollo/client/utilities';
// import _ from 'lodash';

// /**
//  *
//  * @param {*} token - autthentication token, if it exists
//  * @param {*} logoutCallback - logout callback function to be called where necessary
//  */
// const createApolloClient = (token = '', logoutCallback) => {
//     const httpLink = new HttpLink({
//         uri: settings.uri,
//         credentials: 'same-origin',
//     });

//     const authLink = new ApolloLink((operation, forward) => {
//         //   add authorization headers
//         operation.setContext((headers = {}) => ({
//             headers: {
//                 ...headers,
//                 authorization: token || null,
//             },
//         }));

//         return forward(operation);
//     });

//     const logoutLink = onError(({ graphQLErrors, networkError }) => {
//         const errorCode = detectErrors(graphQLErrors);

//         console.log('inside link', { token, graphQLErrors, errorCode });

//         if (['UNAUTHENTICATED'].includes(errorCode)) {
//             logoutCallback && logoutCallback();
//         }
//     });

//     const client = new ApolloClient({
//         link: from([authLink, logoutLink, httpLink]),
//         cache: new InMemoryCache({}),
//     });
//     return client;
// };
// export default createApolloClient;

// //        cache: new InMemoryCache({
// //             typePolicies: {
// //                 TransactionResponse: {
// //                     fields: {
// //                         results: offsetLimitPagination(),
// //                         metadata: {
// //                             merge(prev, incoming) {
// //                                 console.log('incoming metadata==>', { incoming });
// //                                 return incoming;
// //                             },
// //                         },
// //                     },
// //                 },
// //             },
// //         }),

// // cache: new InMemoryCache({
// //     typePolicies: {
// //         Query: {
// //             fields: {
// //                 transactions: {
// //                     keyArgs: [],
// //                     merge(existing = {}, incoming, { args }) {
// //                         console.log('incoming==>', { incoming }, '=====>', { existing }, '====>args', { args });
// //                         if (args && args.page_by != undefined) {
// //                             // args and pageby not undefined so will return this
// //                             console.log('running this part');
// //                             console.log({ existing }, { incoming });
// //                             return {
// //                                 ...incoming,
// //                                 results: [...existing.results, ...incoming.results],
// //                                 metadata: incoming.metadata,
// //                             };
// //                         }
// //                         //calls this at first Initial fetch or refetch ,existig is empty and args and internal are undefined

// //                         return {
// //                             ...existing,
// //                             results: incoming.results,
// //                             metadata: incoming.metadata,
// //                         };
// //                     },
// //                 },
// //             },
// //         },
// //     },
// // }),
