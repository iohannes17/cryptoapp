import { DefaultTheme, configureFonts } from 'react-native-paper';
import { StyleSheet } from 'react-native';

const StyledTheme = {
    layout: {
        backgroundColor: '#FFFFFF',
    },
    colors: {
        // black: 'black',
        black: 'rgba(6,15,33, 1)',
        white: 'white',
        primary: 'rgba(237, 47, 89, 1)',
        // textGrey: 'rgba(114, 114, 144, 0.75)',
        textGrey: '#9292B0',
        // lightGrey: '#EFEFF2',
        lightGrey: '#eff1f1',
        backgroundGrey: '#f8f8f8',
        green: 'rgba(0,190,0,1)',
        red: 'rgba(217,48,37,1)',
        yellow: 'rgba(255,200,56,1)',
        amber: 'rgba(255,191,0, 1)',
        transparent: 'transparent',
        transluscent: {
            grey: 'rgba(145,150,170,0.2)',
            green: 'rgba(0,190,0,0.15)',
            red: 'rgba(217,48,37,0.2)',
            yellow: 'rgba(255,200,56,0.1)',
            primary: 'rgba(237, 47, 89, 0.05)',
            darkBlue: 'rgba(35,67,97, 0.4)',
            black: 'rgba(6,15,33, 0.1)',
            white: 'rgba(255, 255, 255, 0.2)',
        },
        borderColor: '#CACDD3',
    },
    sizes: {
        large: 48,
        medium: 24,
        keyHeight: 72,
        tabIcon: 40,
    },
    borderColor: '#CACDD3',
};

const fontConfig = {
    default: {
        light: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        thin: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        regular: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        medium: {
            fontFamily: 'Larsseit-Medium',
            fontWeight: '500',
        },
        bold: {
            fontFamily: 'Larsseit-Bold',
            // fontWeight: 'bold',
        },
    },
    ios: {
        light: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        thin: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        regular: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        medium: {
            fontFamily: 'Larsseit-Medium',
            fontWeight: '500',
        },
        bold: {
            fontFamily: 'Larsseit-Bold',
            // fontWeight: 'bold',
        },
    },
    android: {
        light: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        thin: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        regular: {
            fontFamily: 'Larsseit-Regular',
            fontWeight: '400',
        },
        medium: {
            fontFamily: 'Larsseit-Medium',
            fontWeight: '500',
        },
        bold: {
            fontFamily: 'Larsseit-Bold',
            // fontWeight: 'bold',
        },
    },
};

const PaperTheme = {
    ...DefaultTheme,
    roundness: 2,
    colors: {
        ...DefaultTheme.colors,
        primary: StyledTheme.colors.primary,
        background: StyledTheme.colors.white,
        accent: StyledTheme.colors.black,
        text: StyledTheme.colors.black,
        disabled: StyledTheme.colors.lightGrey,
        placeholder: StyledTheme.colors.textGrey,
    },
    fonts: configureFonts(fontConfig),
};

const globalStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        // lineHeight: 20,
        backgroundColor: StyledTheme.colors.white,
        height: 54,
        paddingHorizontal: 0,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
    },
    textAreaInput: {
        fontSize: 16,
        // lineHeight: 20,
        backgroundColor: StyledTheme.colors.white,
        height: 120,
        // paddingHorizontal: 4,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
        borderBottomWidth: 1,
        padding: 4,
        // borderRadius: 4,
        borderBottomColor: StyledTheme.colors.lightGrey,
    },
    placeholderInput: {
        fontFamily: fontConfig.default.regular.fontFamily,
        fontWeight: fontConfig.default.regular.fontWeight,
    },
    layout: {
        padding: 16,
    },
    emptyLayout: {
        padding: 16,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    emptyTitle: {
        fontSize: 16,
        fontFamily: fontConfig.default.bold.fontFamily,
        fontWeight: fontConfig.default.bold.fontWeight,
        textAlign: 'center',
    },
    emptyBody: {
        fontSize: 14,
        fontFamily: fontConfig.default.regular.fontFamily,
        fontWeight: fontConfig.default.regular.fontWeight,
        color: StyledTheme.colors.textGrey,
        textAlign: 'center',
    },
    buttonBox: {
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    listStyle: {
        paddingHorizontal: 16,
    },
    headline: {
        fontSize: 32,
        fontFamily: fontConfig.default.bold.fontFamily,
        fontWeight: fontConfig.default.bold.fontWeight,
        letterSpacing: -0.5,
    },
    button: {
        height: 54,
    },
    separator: {
        borderBottomWidth: 0.5,
        height: 1,
        borderBottomColor: StyledTheme.colors.lightGrey,
        marginLeft: 0,
        alignSelf: 'stretch',
    },
    verticalSeparator: {
        borderLeftWidth: 0.5,
        height: '100%',
        borderLeftColor: StyledTheme.colors.lightGrey,
        marginLeft: 0,
        alignSelf: 'stretch',
    },
    bottomSheetWrapper: {
        backgroundColor: StyledTheme.colors.white,
        flex: 1,
        justifyContent: 'center',
    },
    bottomSheetTitle: {
        padding: 10,
        borderBottomWidth: 0.5,
        borderColor: StyledTheme.colors.lightGrey,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottomSheetBody: {
        paddingVertical: 15,
        paddingHorizontal: 20,
        flex: 1,
        justifyContent: 'space-between',
        // paddingTop: 30,
    },
    bottomSheetButtonGroup: {
        alignSelf: 'flex-end',
        width: '100%',
    },
    bottomSheetButton: {
        height: 48,
    },
    bottomSheetText: {
        // flex: 1,
        // flexWrap: 'wrap',
        fontSize: 16,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
    },
    bottomSheetCaption: {
        // flex: 1,
        // flexWrap: 'wrap',
        fontSize: 14,
        fontFamily: fontConfig.default.regular.fontFamily,
        fontWeight: fontConfig.default.regular.fontWeight,
    },
    fab: {
        position: 'absolute',
        alignSelf: 'flex-end',
        right: 10,
    },
    flatListStyle: {
        alignSelf: 'stretch',
        flex: 1,
        height: '100%',
        backgroundColor: 'white',
        // borderTopWidth: StyleSheet.hairlineWidth,
        borderColor: StyledTheme.colors.borderColor,
    },
    search: {
        backgroundColor: StyledTheme.colors.lightGrey,
        flex: 1,
        shadowColor: 'transparent',
        borderRadius: 8,
        height: 40,
    },
    searchInput: {
        fontSize: 16,
        marginLeft: -10,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
    },
    toolbar: {
        paddingHorizontal: 16,
        paddingVertical: 16,
        // paddingBottom: 16,
        // borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: StyledTheme.colors.borderColor,
        backgroundColor: StyledTheme.colors.white,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    headerBackButton: {
        padding: 8,
        paddingHorizontal: 16,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerActionButton: {
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerContentStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 8,
    },
    headerStyle: {
        backgroundColor: StyledTheme.colors.white,
        // borderBottomWidth: StyleSheet.hairlineWidth,
        // borderColor: theme.colors.borderColor,
        borderColor: StyledTheme.colors.borderColor,
        shadowOffset: { height: 0, width: 0 },
        elevation: 0,
        shadowOpacity: 0,
    },
    headerTitleStyle: {
        fontSize: 14,
        color: StyledTheme.colors.black,
        fontFamily: fontConfig.default.bold.fontFamily,
        fontWeight: fontConfig.default.bold.fontWeight,
        textTransform: 'uppercase',
    },
    headerDescriptionStyle: {
        fontSize: 12,
        color: StyledTheme.colors.textGrey,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
    },
    snackbar: {
        color: StyledTheme.colors.white,
        fontSize: 16,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
    },
    itemLabel: {
        color: StyledTheme.colors.black,
        fontFamily: fontConfig.default.medium.fontFamily,
        fontWeight: fontConfig.default.medium.fontWeight,
    },
});

export { StyledTheme, PaperTheme, globalStyles, fontConfig };
