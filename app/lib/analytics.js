// import analytics, { firebase } from '@react-native-firebase/analytics';

// /**
//  *
//  * @param {String} eventName
//  * @param {Object} params
//  * @param {Function} func
//  */

// export const useAnalytics = (name, func, params = {}, includeData = false) => {
//     // const enableAnalytics = () => {
//     //     const f = analytics()['setAnalyticsCollectionEnabled'];
//     //     f && f(true);
//     // };

//     const processAnalytics = (data = {}) => {
//         console.log('logging new event===>', name, { ...params });
//         analytics().logEvent(name, data);
//     };

//     return (...args) => {
//         const payload = { ...params };
//         console.log(payload);
//         if (includeData) {
//             payload.args = args;
//         }
//         enableAnalytics();
//         processAnalytics(payload);
//         return func(...args);
//     };
// };

// // class Analytics {
// //     static init() {
// //         // if (firebase.app().utils().isRunningInTestLab) {
// //         //     analytics().setAnalyticsCollectionEnabled(false);
// //         // } else {
// //         //     analytics().setAnalyticsCollectionEnabled(true);
// //         // }
// //
// //     }

// //     static onSignIn = async (userObject) => {
// //         const { id, email } = userObject;
// //         await Promise.all([analytics().setUserId(id), this.logEvent('sign_in')]);
// //     };

// //     static onSignUp = async (userObject) => {
// //         const { id, email } = userObject;
// //         await Promise.all([
// //             analytics().setUserId(id),
// //             analytics().setUserProperty('created_at', new Date()),
// //             this.logEvent('sign_up'),
// //         ]);
// //     };

// //     /**
// //      * Sets current screen name
// //      * @param {String} screenName
// //      */

// //     static setCurrentScreen = async (screenName) => {
// //         await analytics().setCurrentScreen(screenName, screenName);
// //     };

// //     /**
// //      * Custom event
// //      * @param {String} eventName
// //      * @param {Object} propertyObject
// //      */

// //     static logEvent = async (eventName, propertyObject = {}) => {
// //         await analytics().logEvent(eventName, propertyObject);
// //     };

// //     /**
// //      * Add item to cart
// //      * @param {String} itemName name of item
// //      * @param {String} itemId   item unique code
// //      * @param {number} totalValue value of item
// //      */

// //     static logAddItemToCart = async (itemName, itemId, totalValue) => {
// //         let itemObject = [];
// //         itemObject.push({
// //             item_name: itemName,
// //             item_id: itemId,
// //         });
// //         await analytics().logAddToCart({
// //             items: itemObject,
// //             currency: 'NGN',
// //             value: totalValue,
// //         });
// //     };

// //     /**
// //      *Remove item from cart
// //      * @param {String} itemName name of item
// //      * @param {String} itemId   item unique code
// //      * @param {number} totalValue value of item
// //      */

// //     static logRemoveFromCart = async (itemName, itemId, totalValue) => {
// //         let itemObject = [];
// //         itemObject.push({
// //             item_name: itemName,
// //             item_id: itemId,
// //         });
// //         await analytics().logAddToCart({
// //             items: itemObject,
// //             currency: 'NGN',
// //             value: totalValue,
// //         });
// //     };

// //     /**
// //      * Begin purchase checkout
// //      * @param {array} items
// //      * containing object {
// //      * item_name:'',
// //      * item_id:'',
// //      * item_brand:''
// //      * }
// //      * @param {String} coupon
// //      * @param {number} totalValue total value of all items
// //      */

// //     static logBeginCheckout = async (items = [], totalValue, coupon) => {
// //         await analytics().logBeginCheckout({
// //             currency: 'NGN',
// //             coupon: coupon,
// //             items: items,
// //             value:totalValue
// //         });
// //     };

// //     /**
// //      * Track purchased items
// //      * @param {array} items
// //      * containing object {
// //      * item_name:'',
// //      * item_id:'',
// //      * item_brand:''
// //      * }
// //      *
// //      * @param {number} totalValue
// //      */

// //     static logPurchase = async (items = [], totalValue) => {
// //         await analytics().logPurchase({
// //             currency: 'NGN',
// //             items: items,
// //             value: totalValue,
// //         });
// //     };

// //     static onSignOut = async () => {
// //         await analytics().resetAnalyticsData();
// //     };

// //     static onAppOpen = async () => {
// //         await analytics().logAppOpen();
// //     };

// //     static logSearch = async (word) => {
// //         await analytics().logSearch({
// //             search_term: word,
// //         });
// //     };
// // }

// // export default Analytics;

// // {
// //     /**
// //     eventName: "add_to_cart" | "add_to_wishlist" | "remove_from_cart"
// //     eventParams: { currency?: EventParams["currency"]; items?: EventParams["items"]; value?:
// //  */
// // }
