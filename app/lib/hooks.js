import { useState, useEffect } from 'react';
import 'react-native-get-random-values';
import { v4 as uuidv4 } from 'uuid';
import analytics, { firebase } from '@react-native-firebase/analytics';
import appsFlyer from 'react-native-appsflyer';
import * as Amplitude from 'expo-analytics-amplitude';
import _ from 'lodash';

export function useDebounce(value, delay) {
    // State and setters for debounced value
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(
        () => {
            // Update debounced value after delay
            const handler = setTimeout(() => {
                setDebouncedValue(value);
            }, delay);

            // Cancel the timeout if value changes (also on delay change or unmount)
            // This is how we prevent debounced value from updating if value is changed ...
            // .. within the delay period. Timeout gets cleared and restarted.
            return () => {
                clearTimeout(handler);
            };
        },
        [value, delay], // Only re-call effect if value or delay changes
    );

    return debouncedValue;
}

export function uuid() {
    return uuidv4();
}

/**
 *
 * @param {String} name
 * @param {Function} func
 * @param {Object} params
 * @param {Boolean} includeData
 */
export const useAnalytics = (name, func, params = {}, includeData = false) => {
    const processAnalytics = (data = {}) => {
        analytics().logEvent(name, data);
    };

    return (...args) => {
        const payload = { ...params };
        if (includeData) {
            payload.args = args;
        }
        processAnalytics(payload);
        return func(...args);
    };
};

export const initAppsFlyer = (name, func, params = {}, includeData = false) => {
    const processEvent = (data = {}) => {
        appsFlyer.logEvent(name, data);
    };
    const amplitudeEvent = (properties = {}) => {
        if (_.isEmpty(properties)) {
            Amplitude.logEvent(name);
        } else {
            Amplitude.logEventWithProperties(name, properties);
        }
    };

    const googleAnalytics = (data = {}) => {
        analytics().logEvent(name, data);
    };

    return (...args) => {
        const payload = { ...params };
        if (includeData) {
            payload.args = args;
        }
        processEvent(payload);
        amplitudeEvent(payload);
        googleAnalytics(payload);
        return func(...args);
    };
};
