export const loadMoredata = (resultName, metadata, fetchMore) => {
    const {
        page_by: { next_page },
    } = metadata;

    // if (next_page) {
    //     console.log('calling load more');
    //     fetchMore({
    //         variables: { page_by: { page: next_page } },
    //     });
    // }

    if (next_page) {
        fetchMore({
            variables: { page_by: { page: next_page } },
            updateQuery: (previousResult, { fetchMoreResult }) => {
                if (!fetchMoreResult) {
                    return previousResult;
                }
                return {
                    ...fetchMoreResult,
                    [`${resultName}`]: {
                        ...fetchMoreResult[resultName],
                        results: [...previousResult[resultName].results, ...fetchMoreResult[resultName].results],
                        metadata: fetchMoreResult[resultName].metadata,
                    },
                };
            },
        });
    }
};
