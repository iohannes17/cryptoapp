import React, { Fragment, useCallback, useEffect, useState, useRef, useContext } from 'react';
import { Alert } from 'react-native';

export const onBackgroundMessage = async (message) => {
    console.log('background message', JSON.stringify(message));
};
