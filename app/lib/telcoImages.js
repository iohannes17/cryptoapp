// import ElectricityLogo from "app/images/icons/payments/electricity.svg";

import MTNLogo from 'app/images/icons/telcos/mtn.svg';
import AirtelLogo from 'app/images/icons/telcos/airtel.svg';
import GloLogo from 'app/images/icons/telcos/glo.svg';
import MobileLogo from 'app/images/icons/telcos/9mobile.svg';
import AirtimeIcon from 'app/images/tabs/airtime.svg';
import ElectricityIcon from 'app/images/tabs/electricity.svg';

export default function getTelcoImage(code = '00000') {
    const imageCode = code;
    let image = ElectricityIcon;
    switch (imageCode) {
        case '114753737':
            image = AirtimeIcon;
            break;
        case '114743731':
            image = AirtimeIcon;
            break;
        case '114713716':
            image = AirtimeIcon;
            break;
        case '114733723':
            image = AirtimeIcon;
            break;
        default:
            image = ElectricityIcon;
            break;
    }

    return image;
}
