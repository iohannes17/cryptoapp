const tintColor = '#ffffff';

export default {
  tintColor,
  tabIconDefault: "rgb(255, 192, 0)",
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
