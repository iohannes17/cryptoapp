import {StyleSheet} from 'react-native';

export const styles= StyleSheet.create({
  topMargin:{marginTop:10},
  content:{padding:10,backgroundColor:"white"},
  heading:{fontSize:32,fontWeight:"400",marginBottom:20},
    
});