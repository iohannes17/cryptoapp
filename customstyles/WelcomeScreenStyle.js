import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen'; 

export const welcomStyle = StyleSheet.create({
   container:{
       justifyContent:'center',
       alignItems:'center',
       flexGrow:1
   },

   logotext:{
     color:'gold',
     marginTop:6,
     fontWeight:"900",
     fontSize:20,
     fontFamily:'spectral-semi',
     alignSelf:'center'
   },

   undertext:{
       color:'#fefefe',
       fontWeight:"200",
       fontSize:hp('1.5%'),
       marginTop:10,
       fontWeight:"200",
       alignSelf:'center',
       fontFamily:'spectral-italic',

   },
   containerContent:{
       width:wp('100%'),
       borderBottomColor:'gray',
       backgroundColor:'#16202a',
      
   },

   secondContainerContent:{
    flexDirection:'row',
    width:wp('100%'),
    flex:2,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#16202a',
    

   },

   buttonText:{
      fontFamily:'spectral-light-italic',
      fontSize:hp('1.6%'),  
      color:'#7f7f7f' , 
      paddingLeft:wp('3%'),
      paddingRight:wp('3%'),
   },

//    footerText:{
//     fontFamily:'spectral-light-italic',
//     fontSize:hp('1.5%'),  
//     color:'#f5f5f5'  
//  },

//  footerTextFpassword:{
//     fontFamily:'spectral-light-italic',
//     fontSize:hp('1.9%'),  
//     color:'#7f7f7f' ,
//     paddingLeft:5,
//     paddingRight:5, 
   
//  },

 footerStyle:{
    alignContent:'center',
    alignItems:'center',
    flexDirection:'row',
    width: wp('100%'),
    height:hp('5.5%'),
    backgroundColor:'#16202a'
 },

 Secondcontainer:{
    height:hp('6%'),
    width:wp('100%'),
 },

 thirdContainer:{
    backgroundColor:'#16202a',
    justifyContent:'center',
    alignContent:'center',
    alignItems:'center',
    height:hp('58%'),
    width:wp('100%'),
 },

 thirdContainercontent:{
    justifyContent:'center',
    alignContent:'center',
    alignItems:'center'
 },

 loginUndertext:{
     marginTop:hp('4%'),
     marginLeft:wp('4%'),
     fontFamily:'spectral-light-italic',
     fontSize:hp('1.6%'),  
     color:'#7f7f7f' , 
    
 }



});