import SignedOutNavigator from "./SignedOutNavigator";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

const Navigations = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name={"signedOut"} component={SignedOutNavigator} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default Navigations;
