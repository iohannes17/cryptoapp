import React from "react";
import { Platform } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

const TabScreens = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const MoreStack = createStackNavigator();
const AirtimeStack = createStackNavigator();
const TransferScreenCryptoStack = createStackNavigator();
const HistoryStack = createStackNavigator();
