import React from "react";
import WelcomeScreen from "screens/Welcome";
import { createStackNavigator } from "@react-navigation/stack";
const Stack = createStackNavigator();

const SignedOutNavigator = () => (
  <Stack.Navigator initialRouteName={"home"} headerMode={"none"}>
    <Stack.Screen
      name="welcome"
      component={WelcomeScreen}
      options={{ title: "" }}
    />
  </Stack.Navigator>
);

export default SignedOutNavigator;
