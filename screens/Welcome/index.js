// list your chat history
import React, { useState, useRef, Fragment, useEffect } from "react";
import { Layout, Spacer } from "app/components";
import {
  View,
  ScrollView,
  StyleSheet,
  Pressable,
  Modal,
  KeyboardAvoidingView,
  Keyboard,
  Platform,
  TouchableWithoutFeedback,
  SectionList,
  FlatList,
} from "react-native";
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import _ from "lodash";

import {
  Button,
  Subheading,
  Headline,
  Paragraph,
  TextInput,
  ActivityIndicator,
  Text,
  List,
  Colors,
  Portal,
  FAB,
  Caption,
  Searchbar,
} from "react-native-paper";
import {
  globalStyles,
  fontConfig,
  StyledTheme as theme,
} from "app/config/theme";

const Page = ({ navigation, route }) => {
  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <Pressable
          style={globalStyles.headerBackButton}
          onPress={() => navigation.goBack()}
        >
          <MaterialCommunityIcons
            name={"arrow-left"}
            color={theme.colors.black}
            size={28}
          />
        </Pressable>
      ),
      title: "Transactions",
      headerStyle: {
        ...globalStyles.headerStyle,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: theme.colors.borderColor,
        // borderColor: StyledTheme.colors.transparent,
        // shadowOffset: { height: 0, width: 0 },
      },
      headerTitleStyle: {
        ...globalStyles.headerTitleStyle,
        textTransform: "uppercase",
      },
    });
  });

  return (
    <Layout style={[globalStyles.emptyLayout]} edges={["left", "right"]}>
      <Paragraph style={globalStyles.emptyTitle}>{"Crypto App"}</Paragraph>
      <Paragraph style={globalStyles.emptyBody}>
        {"Welcome to CryptoVille."}
      </Paragraph>
      <Spacer height={16} />
      <Button
        theme={{ colors: { primary: theme.colors.black }, roundness: 24 }}
        style={[styles.miniButton]}
        contentStyle={styles.miniButton}
        labelStyle={styles.buttonLabelStyle}
        mode={"contained"}
      >
        {"WELCOME"}
      </Button>
    </Layout>
  );
};

const styles = StyleSheet.create({
  buttonLabelStyle: {
    fontSize: 13,
  },
  miniButton: {
    height: 48,
  },
});

export default Page;
